# tree-sitter-t32-playground

Syntax Tree Playground for [tree-sitter-t32](https://codeberg.org/xasc/tree-sitter-t32). Implementation inspired by [tree-sitter-rescript-web-playground](https://github.com/aspeddro/tree-sitter-rescript-web-playground).

The playground can be found [here](https://xasc.codeberg.page/tree-sitter-t32-playground/).
