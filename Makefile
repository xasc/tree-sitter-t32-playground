.PHONY: build update-parser update-queries

build:
	yarnpkg run webpack

update-parser:
	sudo $(MAKE) -C ../tree-sitter-t32 build-wasm
	cp ../tree-sitter-t32/tree-sitter-t32.wasm ./

update-queries:
	QUERY=`cat ../tree-sitter-t32/queries/highlights.scm)` envsubst '$$QUERY' < playground.template.js 1> src/playground.js
